import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecruiterFormComponent } from './recruiter-form/recruiter-form.component';
import { InterviewerFeedbackFormComponent } from './interviewer-feedback-form/interviewer-feedback-form.component';
import { CandidateFeedbackFormComponent } from './candidate-feedback-form/candidate-feedback-form.component';
import { RecruiterServiceService } from './recruiter-service.service';
import { FormsModule } from '@angular/forms';
import { CommonService } from './common.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    RecruiterFormComponent,
    InterviewerFeedbackFormComponent,
    CandidateFeedbackFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule ,
    HttpClientModule    
  ],
  providers: [RecruiterServiceService, CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
