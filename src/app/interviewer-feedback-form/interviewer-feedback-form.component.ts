import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-interviewer-feedback-form',
  templateUrl: './interviewer-feedback-form.component.html',
  styleUrls: ['./interviewer-feedback-form.component.scss']
})
export class InterviewerFeedbackFormComponent implements OnInit {
  items = ["candidateName","slot","techskills","commskills","analyticalskills","problemstatement","finalreport"]
  constructor() { }
  final:any;
  name:any;
  slot:any;
  techskills: any;
  commskills: any;
  anaskills: any;
  problemsolve: any;
  body: any;
  feedback: boolean | undefined = false;
  scale= ["1","2","3","4","5"]

  ngOnInit(): void {

}
  submit(){
    this.body = {
      "name":this.name,
      "slot":this.slot,
      "techskills":this.techskills,
      "commskills":this.commskills,
      "anaskills":this.anaskills,
      "problemsolve":this.problemsolve,
      "final":this.final
    }
    console.log(this.body)
    this.feedback = true;
// this.comm.postMethod("","").subscribe(res=>{

// })


   }

}
