import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewerFeedbackFormComponent } from './interviewer-feedback-form.component';

describe('InterviewerFeedbackFormComponent', () => {
  let component: InterviewerFeedbackFormComponent;
  let fixture: ComponentFixture<InterviewerFeedbackFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterviewerFeedbackFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InterviewerFeedbackFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
