import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecruiterFormComponent } from './recruiter-form/recruiter-form.component';
import { CandidateFeedbackFormComponent } from './candidate-feedback-form/candidate-feedback-form.component';
import { InterviewerFeedbackFormComponent } from './interviewer-feedback-form/interviewer-feedback-form.component';

const routes: Routes = [
  {
    path: "",
    component: RecruiterFormComponent
  },
  {
    path: "recruiter",
    component: RecruiterFormComponent
  },
  {
    path: "candidate-feedback",
    component: CandidateFeedbackFormComponent
  },
  {
    path: "Interviewer-feedback",
    component: InterviewerFeedbackFormComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
