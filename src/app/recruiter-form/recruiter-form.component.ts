import { Component, OnInit } from '@angular/core';
import { RecruiterServiceService } from '../recruiter-service.service';

@Component({
  selector: 'app-recruiter-form',
  templateUrl: './recruiter-form.component.html',
  styleUrls: ['./recruiter-form.component.scss']
})
export class RecruiterFormComponent implements OnInit {
  interviewer: any;
  interviewerSlot: any;
  candidateSlot: any;
  candidateName: any;
  recruitersData: any;
  getRecruiters: any;
  slotsList: any;
  interviewersList: any;
  candidatesList: any;
  listOfParams: any;
  paramsData: any;
  dataList: any = [];
  constructor(private recruiterServiceService: RecruiterServiceService) {
  }

  ngOnInit(): void {
    this.slotsList = ["10:00AM - 11:00AM", "11:00AM - 12:00PM", "12:00PM - 01:00PM", "01:00PM - 02:00PM"];;
    this.interviewersList = ["Interviewer1", "Interviewer2", "Interviewer3", "Interviewer4"];
    this.candidatesList = ["Raja", "Sekhar", "Vijay", "Sakir"];
    this.listOfParams = ["Candidate Name", "Candidate Slot", "Interviewer Name", "Interviewer Slot", "Status"];
   // this.getRecruitersData();
  }

  save() {
   // this.dataList = [];
    const obj = {
      "interviewer": this.interviewer,
      "slot1": this.interviewerSlot,
      "candidate": this.candidateName,
      "slot2": this.candidateSlot,
      "Status": "scheduled"
    }
    this.dataList.push(obj);
    // this.recruiterServiceService.recruiterDataSave(obj).subscribe((res: any) => {
    //    this.recruitersData = res;
    // }, (err: any) => {
    //   console.log(err);
    // })
  }

  getRecruitersData(){
    this.recruiterServiceService.getRecruiterData().subscribe((res: any) => {
      this.getRecruiters = res;
   }, (err: any) => {
     console.log(err);
   })
  }



}
